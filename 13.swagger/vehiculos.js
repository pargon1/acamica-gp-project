const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const server = express();
const puerto = 5050;

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Vehiculos API',
            version: '1.0.0',
            description: 'Una API para gestionar vehículos'
        }
    },
    apis: ['./vehiculos.js']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);
server.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));
server.use(express.json());

let vehiculos = [
    {
        marca: 'BMW',
        modelo: 2020,
        fechaFabricacion: '10/06/2019',
        cantidadPuertas: 5,
        disponibleParaVenta: true
    }
];
/**
 * @swagger
 * /vehiculos:
 *  get:
 *    description: Endpoint para obtener todos los vehículos registrados
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 * 
 */

 
 
server.get('/vehiculos', (req, res) => {
    res.json(vehiculos);
});


/**
 * @swagger
 * /vehiculos:
 *  post:
 *    description: Endpoint para registrar vehículos
 *    parameters:
 *    - name: body
 *      description: Cuerpo de vehículo
 *      in: body
 *      required: true
 *      type: string
 *      example: { marca: "string", modelo: number, fechaFabricacion: "string (YY/MM/DD)", cantidadPuertas: number, disponibleParaVenta: boolean}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 * 
 */
 server.post('/vehiculos', (req, res) => {
    vehiculos.push(req.body);
    console.log(vehiculos);
    res.send(  'Agregado');
});


server.listen(puerto, () => {
    console.log(`Servidor en ejecution en el puerto ${puerto}`);
});