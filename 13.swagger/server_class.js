const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc'); //Paquete de swagger
const swaggerUI = require('swagger-ui-express'); //

const server = express();

//Opciones de inicialización de la documentación Swagger
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "My App API",
            version: "1.0.0",
            description: "Esta es la primera versión de mi API" 
        }
    },
    apis: ['./server.js']
}

//Swagger
const swaggerDocs = swaggerJsDoc(swaggerOptions);

const personas = [
    {
        nombre: 'Andre',
        edad: 25
    },
    {
        nombre: 'Augus',
        edad: 19
    },
    {
        nombre: 'Andre',
        edad: 30
    }
];

server.use('/personas', (req, res, next) => {
    console.log('Cualquier operacion de personas pasa por aquí');
    next();
});

//Swagger
server.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));

server.use(express.json());
server.use(logger);

function logger(req, res, next) {
    console.log(`Ruta: ${req.path} - Metodo: ${req.method} - Body: ${JSON.stringify(req.body)} - Params: ${JSON.stringify(req.params)}`);
    next();
}

const imprimirFecha = (req, res, next) => {
    console.log(Date.now());
    next();
}

const imprimirParOImpar = (req, res, next) => {
    let numeroAleatorio = Math.floor(Math.random() * 100);
    if (numeroAleatorio % 2 === 1){
        res.status(406).json({mensaje: 'La prueba de pares e impares no pasó.'});
    } else {
        next();
    }
}

server.get('/saludo', imprimirFecha, imprimirParOImpar, (req, res) => {
    console.log('Fui invocado');
    res.send('Hola a todos. Mi primer endpoint');
});

/**
 * @swagger
 * /personas:
 *  get:
 *    description: Obtener un listado de personas o una única persona.
 *    parameters:
 *    - name: nombre
 *      description: Nombre de una persona existente en la lista de personas.
 *      in: query
 *      required: false
 *      type: string
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *      404:
 *        description: Persona no encontrada
 *
 */
server.get('/personas', (req, res) => {
    console.log(JSON.stringify(req.query));
    if (Object.keys(req.query).length !== 0) {
        let encontrado = personas.find((persona) => persona.nombre === req.query.nombre);
        if (!encontrado) {
            res.status(404).json({mensaje: "Persona no encontrada"});
        } else {
            res.json(encontrado);
        }
    } else {
        res.send(personas);
    }
});

/**
 * @swagger
 * /personas:
 *  post:
 *    description: Permite crear una persona en la lista.
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: body
 *      description: Cuerpo de una persona.
 *      in: body
 *      required: true
 *      type: string
 *      example: {nombre: string, edad: number, email: "string"}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *    
 *
 */
server.post('/personas', (req, res) => {
    console.log(req.body);
    personas.push(req.body);
    res.json({mensaje: 'Nombre insertado'});
});

server.put('/personas/:indice', (req, res) => {
    personas[parseInt(req.params.indice)] = req.body;
    res.json({mensaje: 'Actualizamos un nombre'});
});


server.delete('/personas', (req, res) => {
    res.send('Borrar un nombre');
});

server.listen(3000, () => {
    console.log('Servidor en ejecución...');
});