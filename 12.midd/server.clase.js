const express = require('express');
const server = express();

const personas = [
    {
        nombre: 'Andre',
        edad: 25
    },
    {
        nombre: 'Augus',
        edad: 19
    },
    {
        nombre: 'Andre',
        edad: 30
    }
];

const logger = function(req, res, next) {
    console.log(`Ruta: ${req.path} - Metodo: ${req.method} - Body: ${JSON.stringify(req.body)} - Params: ${JSON.stringify(req.params)}`);
    next();
}

const imprimirFecha = (req, res, next) => {
    console.log(Date.now());
    next();
}

const imprimirParOImpar = (req, res, next) => {
    let numeroAleatorio = Math.floor(Math.random() * 100);
    if (numeroAleatorio % 2 === 1){
        res.status(406).json({mensaje: 'La prueba de pares e impares no pasó.'});
    } else {
        next();
    }
}

server.use(express.json());
server.use(logger);

server.get('/saludo', imprimirFecha, imprimirParOImpar, (req, res) => {
    console.log('Fui invocado');
    res.send('Hola a todos. Mi primer endpoint');
});

server.get('/personas', (req, res) => {
    console.log(JSON.stringify(req.query));
    if (Object.keys(req.query).length !== 0) {
        let encontrado = personas.find((persona) => persona.nombre === req.query.nombre);
        if (encontrado) {
            res.status(404).json({mensaje: "Persona no encontrada"});
        } else {
            res.json(encontrado);
        }
    } else {
        res.send(personas);
    }
});

server.post('/personas', (req, res) => {
    console.log(req.body);
    personas.push(req.body);
    res.json({mensaje: 'Nombre insertado'});
});

server.put('/personas/:indice', (req, res) => {
    personas[parseInt(req.params.indice)] = req.body;
    res.json({mensaje: 'Actualizamos un nombre'});
});


server.delete('/personas', (req, res) => {
    res.send('Borrar un nombre');
});

server.listen(3000, () => {
    console.log('Servidor en ejecución...');
});