const express = require('express');
const logger = require('./appmid');
const app = express();


app.use(express.json);

app.post('/estudiantes', (req, res) =>{
    res.status(201).send();
})
/*
const logger = (req, res, next) => {

    console.log(`request HTTP method: ${req.method}`);
    next();
    };
*/
app.use(logger);

const esAdministrador = (req, res, next) => {
    
    const usuarioAdministrador = true;
    
        if(usuarioAdministrador ) {
    
            console.log('El usuario está correctamente logueado.');
    
        next();
    
    } else {
    
        res.status(418).send('No está logueado');
    
        }
    
};

const otroMid = (req, res, next) => {
    console.log('nuevo mid');
    //next();
    res.send();
}

app.get('/estudiantes', esAdministrador, otroMid, (req, res) => {
    
    console.log( JSON.stringify(req.query));

    res.send([
      {id: 1, nombre: "Lucas", edad: 35}
    ])
});

app.post('/estudiantes', esAdministrador, otroMid, (req, res) => {
    let nom = req.body;
    console.log( JSON.stringify(req.query));

    res.send([
        {id: 1, nombre: "Lucas", edad: 35}
])
});

app.put('/estudiantes/:indice', esAdministrador, otroMid, (req, res) => {
    console.log( req.params.indice);

    res.json('hola');
});

app.get('/cursos', esAdministrador, (req, res) => {
    res.send([
    {id: 100, nombre: "Back-End"},
    {id: 200, nombre: "Front-End"},
    {id: 300, nombre: "Full-Stack"}
])
});


app.listen(5001, ()=> console.log('Listo en 5001'));
