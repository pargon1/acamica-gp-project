function sumando(v1, v2){
    return v1+v2;
}

function restando(v1, v2){
    return v1 - v2;
}

function mult (v1, v2) {
    return v1* v2;
}

function div(v1, v2){
    if (v2===0){
        throw err;
    }
    return v1/v2;
}

exports.suma = sumando;
exports.resta = restando;
exports.multiplica = mult;
exports.divide = div;
