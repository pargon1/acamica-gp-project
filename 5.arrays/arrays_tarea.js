var marcas = new Array();

document.getElementById("boton").addEventListener("click", function(){

    let marca = document.getElementById("marca").value;

    if(!(marca === "")){
        marcas.push(marca);
    }

    // rearmar lista
    rearmarList();
});

document.getElementById("delpri").addEventListener("click", function(){

    marcas.shift();
    // rearmar lista
    rearmarList();

});

document.getElementById("delult").addEventListener("click", function(){

    marcas.pop();

    // rearmar lista
    rearmarList();

});


function rearmarList(){
    let fila = "";
    for(i=0; i<marcas.length; i++){
        fila += "<li>" + marcas[i] +"</li>";
    }    
    document.getElementById("lista").innerHTML = fila;
}