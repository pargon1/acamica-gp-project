var dd = document.getElementById("butpuede").addEventListener("click", function(){

    var nom = document.getElementById("nom").value;
    var ape = document.getElementById("ape").value;
    var edad = parseInt(document.getElementById("edad").value);
    var lic = document.getElementById("lic").value;
    var fecha = parseInt( document.getElementById("fechaexp").value);
    
    var sit = true;

    if(nom === ""){
        console.log("Debe especificar Nombre");
        sit = false;
    }
    if(ape === ""){
        console.log("Debe especificar Apellido");
        sit = false;
    }
    if( !( edad > 0 )){
        console.log("Debe especificar Edad");
        sit = false;
    }
    if(! (fecha > 20000101 && fecha <21000101) ){
        console.log("Debe especificar Fecha Exp.");
        sit = false;
    }

    if (sit && edad < 18){
        console.log("Debe ser mayor de 18 años");
        sit = false;
    }
    if (sit && lic != "Si"){
        console.log("Debe tener Lic.");
        sit = false;
    }
    var hoy = 20210508;
    if (sit && (fecha < hoy)){
        console.log("Licencia Vencida");
        sit = false;
    }

    console.log("Resultado: " + (sit ? "correcto" : "hay errores"));
});