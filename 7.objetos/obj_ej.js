let mayorEdad = 18;

class Persona{
    
    constructor(nombre, apellido, edad){
        this.nombre = nombre;
        this.apellido= apellido;
        this.edad=edad;
    }

    fullname(){
        let fullnombre = `${this.nombre} ${this.apellido}`;
        return fullnombre ;
    }
    esMayor(){
        return (this.edad >= mayorEdad);
    }
}

let persona1 = new Persona('Aldo', 'Valente', 22);
console.log(persona1.fullname());
console.log(persona1.esMayor());
