
class Estudiante{
    constructor(nombre, apellido, mail, nacionalidad){
        this.apellido = apellido;
        this.mail = mail;
        this.nombre = nombre;
        this.nacionalidad = nacionalidad;
    }
    mostrar(){
        console.log(`Estudiante: ${this.apellido}, ${this.nombre}, mail: ${this.mail}, nacionalidad: ${this.nacionalidad}`);
    }
}

let estudiante1 = new Estudiante("Andrea", "Campanella", "anrocampa@gmail.com", "Chilena");
let estudiante2 = new Estudiante(nombre="Augusto", apellido="Lallana", mail="aaauuugustolallana@gmail.com", nacionalidad="Colombiana");
let estudiante3 = new Estudiante(nombre="Federico", apellido="Alvarez", mail="federicoalvarez18@gmail.com", nacionalidad="Argentina");

function getEst(){

    let estudiantes = new Array();
    estudiantes.push(estudiante1);
    estudiantes.push(estudiante2);
    estudiantes.push(estudiante3);

    let chrEst = estudiantes.join('\n');
    return estudiantes; //  chrEst;
}


module.exports.estudiantes = getEst();

//exports.estudiantes = getEst;

/*
estudiante1.mostrar();
estudiante2.mostrar();
estudiante3.mostrar();
*/
