const express = require('express');
const logger = require('./lib/appmid');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const { json } = require('express');
const puerto = 5050;

const server = express();

//Opciones de inicialización de la documentación Swagger
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "Delilah Restó API",
            version: "1.0.0",
            description: "by Gonzalo Parra" 
        }
    },
    apis: ['./server.js']
}
//Swagger
const swaggerDocs = swaggerJsDoc(swaggerOptions);
//Swagger
server.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));

//
// use basicos
//
server.use(express.json());
server.use(logger);

//
// clases
//
class Usuario{
    constructor(nombre, apellido, mail, direenvio, telefono, userid, password){
        this.nombre = nombre;
        this.apellido= apellido;
        this.mail=mail;
        this.direenvio=direenvio;
        this.telefono=telefono;
        this.userid=userid;
        this.password=password;
        this.admin=false;
    }
    setAdmin(){
        this.admin=true;
    }
}
class Pedido{
    constructor(numero, fecha, userid, codmediopago, detalle){
        this.numero=numero;
        this.fecha=fecha;
        this.userid=userid;
        this.codmediopago=codmediopago;
        this.estado='Pendiente';
        this.detalle=detalle;
    }
    setEstado(estado){
        this.estado = estado;
    }
}
class DetallePedido{
    constructor(codproducto, cantidad){
        this.codproducto=codproducto;
        this.cantidad=cantidad;
    }
}
class Producto{
    constructor(codigo, descripcion, precio){
        this.codigo=codigo;
        this.descripcion=descripcion;
        this.precio=precio;
    }
}
class MedioPago{
    constructor(codigo, descripcion){
        this.codigo=codigo;
        this.descripcion=descripcion;
    }
}
//
// variables globales
//
let usuarioAdm = new Usuario('ADMIN', '', 'ADMIN@mail.com', '', '', 'ADMIN', 'ADMIN123');
usuarioAdm.setAdmin();
let usuarios =[usuarioAdm];
let pedidos = [];
let productos = [];
let mediospago = [];
let nroPedido = 1000;
let usuarioConectado;

//
// funciones
//

//
// middelware
//
const validaNuevoUsuario = (req, res, next) => {

    // busca mail
    let usumail = usuarios.find( (usu) => usu.mail === req.body.mail); 

    if (usumail){
        res.status(403).send('Ya existe registro de ese mail');
    }else{
        next();
    }
}
const validaNuevoPedido = (req, res, next) => {

    //******************
    // validar existe productos, medios de pago
    let err = false;

    if(err){
        res.send('Error ... ');
    }else{
        next();
    }
}
const validaSesion = (req, res, next) => {

    let ususesion = usuarios[req.header('sesionid')];

    if (ususesion){
        next();
    }else{
        res.status(404).send('No existe sesión con ese id');
    }
}
const validaUsuarioAdmin = (req, res, next) => {

    let husu = usuarios[ req.header('sesionid')];
    console.log(husu);

    if ( husu ){
        if ( husu.admin ){
            next();
        }else{
            res.status(401).send('Usuario no es Admin');
        }
    }else{
        res.status(404).send('Usuario no existe');
    }
}
const validaProducto = (req, res, next) => {
    // ************* hacer

    next();
}

//
// endpoints
//
/**
 * @swagger
 * /usuario:
 *  post:
 *    summary: Crear usuario
 *    description: Permite crear una cuenta de usuario.
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: body
 *      description: Cuerpo de una persona.
 *      in: body
 *      required: true
 *      type: string
 *      example: {nombre: String, apellido: String, mail: String, direccionenvio: String, telefono: String, userid: String, password: String}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Usuario Creado
 */
server.post('/usuario', validaNuevoUsuario, (req, res) => {

    usuarios.push(req.body);
    console.log(`despues: ${usuarios}`);

    res.send('Usuario Creado');
});
/**
 * @swagger
 * /login:
 *  post:
 *    summary: Login del usuario
 *    description: Permite iniciar sesión al usuario.
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: body
 *      description: Cuerpo de una persona.
 *      in: body
 *      required: true
 *      type: string
 *      example: {userid: String, password: String}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Id Login
 *      401:
 *        description: Password incorrecto
 *      404:
 *        description: Usuario no encontrado 
 */
server.post('/login', (req, res) => {
    let jsonusu = req.body;

    // buscar usuario
    let usuarioEncontrado = usuarios.find( (usu) => usu.userid === jsonusu.userid);

    console.log(usuarioEncontrado);

    if (usuarioEncontrado){
        if (usuarioEncontrado.password === jsonusu.password){
            let it = usuarios.indexOf(usuarioEncontrado);            
            res.send(`${it}`);
        }else{
            res.status(401).send('Password incorrecto');
        }
    }else{
        res.status(404).send('Usuario no encontrado');
    }
});
/**
 * @swagger
 * /pedido:
 *  post:
 *    summary: Nuevo Pedido
 *    description: Permite crear un pedido a un usuario.
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un pedido.
 *      in: body
 *      required: true
 *      type: string
 *      example: {codmediopago: String, detalle: {codproducto: String, cantidad: Number}}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 */
server.post('/pedido', validaSesion, validaNuevoPedido, (req, res) => {

    const jsonped = req.body;
    const fecha = Date();
    const usuheader = usuarios[req.header('sesionid')].userid;

    nroPedido = nroPedido+1;    

    let ped = new Pedido(nroPedido, fecha, usuheader, jsonped.codmediopago, jsonped.detalle);

    pedidos.push(ped);
    console.log(`despues: ${JSON.stringify(pedidos)}`);
    
    res.send('Pedido Creado');
});
/**
 * @swagger
 * /pedido:
 *  put:
 *    summary: Actualiza Pedido
 *    description: Permite editar un pedido a un usuario.
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un pedido.
 *      in: body
 *      required: true
 *      type: string
 *      example: {numero: Number, codmediopago: String, detalle: {codproducto: String, cantidad: Number}}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *      404:
 *        description: Pedido no encontrado
 *      405:
 *        description: Usuario no es propietario del numero de pedido
 *      406:
 *        description: Pedido Cerrado
 */
 server.put('/pedido', validaSesion, validaNuevoPedido, (req, res) => {

    const jsonped =  req.body ;    

    const usuheader = usuarios[req.header('sesionid')].userid;

    let numpedido = Number.parseInt( jsonped.numero);

    const index = pedidos.findIndex( (ped) => ped.numero === numpedido);

    if (index > -1){
        if ( pedidos[index].userid === usuheader){
            if (pedidos[index].estado === "Pendiente"){
                pedidos[index].codmediopago = jsonped.codmediopago;
                pedidos[index].detalle = jsonped.detalle;

                console.log(`despues: ${JSON.stringify(pedidos)}`);
                res.send('Petición exitosa');
            }else{
                res.status(406).send('Pedido Cerrado');
            }
        }else{
            res.status(405).send('Usuario no es propietario del numero de pedido');
        }
    }else{
        res.status(404).send('Pedido no encontrado');
    }
});
/**
 * @swagger
 * /mispedidos:
 *  get:
 *    summary: Lista pedidos del usuario
 *    description: Obtener un listado de pedidos del usuario.
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *      404:
 *        description: Usuario no encontrado
 *
 */
server.get('/mispedidos', validaSesion, (req, res) => {
    
    const usuheader = usuarios[req.header('sesionid')];

    let encontrado = pedidos.filter((ped) => ped.userid === usuheader.userid);
    res.send(JSON.stringify( encontrado));
});
/**
 * @swagger
 * /pedidos:
 *  get:
 *    summary: Todos los pedidos
 *    description: Obtener un listado con todos los pedidos (sólo usuario Admin puede invocar).
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *      401:
 *        description: Usuario no es Admin
 *      404:
 *        description: Usuario no existe
 *
 */
 server.get('/pedidos', validaSesion, validaUsuarioAdmin, (req, res) => {
    
    res.send(JSON.stringify(pedidos));
});
/**
 * @swagger
 * /pedido/estado:
 *  put:
 *    summary: Actualiza estado del pedido
 *    description: Permite cambiar estado de un pedido (sólo usuario Admin).
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Estado nuevo [Pendiente, Confirmado, En preparación, Enviado, Entregado]
 *      in: body
 *      required: true
 *      type: string
 *      example: {numero: Number, estado: String}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *      404:
 *        description: Pedido no encontrado
 */
server.put('/pedido/estado', validaSesion, validaUsuarioAdmin, (req, res) => {

    let pedidoUpd = req.body;
    console.log(pedidoUpd);

    let index = pedidos.findIndex( (ped) => ped.numero === pedidoUpd.numero);
    
    if(index> -1){
        pedidos[index].estado = pedidoUpd.estado;
        res.send('Peticion exitosa');
    }else{
        res.status(404).send('Pedido no encontrado');
    }
});
/**
 * @swagger
 * /producto:
 *  post:
 *    summary: Crear producto
 *    description: Permite crear un producto (sólo usuario Admin).
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un producto.
 *      in: body
 *      required: true
 *      type: string
 *      example: {codproducto: String, descripcion: String, precio: Number}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Producto Creado
 *      405:
 *        description: Código existente
 */
 server.post('/producto', validaSesion, validaUsuarioAdmin, (req, res) => {

    const jsonprod = req.body;

    if(productos.find((prd) => prd.codproducto=== jsonprod.codproducto)){
        res.status(405).send('Código existente');
    }else{
        productos.push(jsonprod);
        console.log(`despues: ${JSON.stringify(productos)}`);
        
        res.send('Producto Creado');
    }
});
/**
 * @swagger
 * /producto:
 *  put:
 *    summary: Actualizar producto
 *    description: Permite editar un producto (sólo usuario Admin).
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un producto.
 *      in: body
 *      required: true
 *      type: string
 *      example: {codproducto: String, descripcion: String, precio: Number}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Petición exitosa
 *      404:
 *        description: Código no encontrado
 */
 server.put('/producto', validaSesion, validaUsuarioAdmin, (req, res) => {

    const jsonprod = req.body;

    let index = productos.findIndex( (prod) => prod.codproducto === jsonprod.codproducto);

    if (index > -1){
        productos[index].descripcion = jsonprod.descripcion;
        productos[index].precio = jsonprod.precio;

        console.log(`despues: ${JSON.stringify(productos)}`);    
        
        res.send('Petición exitosa');
    }else{
        res.status(404).send('Código no encontrado')
    }

});
/**
 * @swagger
 * /producto:
 *  delete:
 *    summary: Elimina producto
 *    description: Permite eliminar un producto (sólo usuario Admin).
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un producto.
 *      in: body
 *      required: true
 *      type: string
 *      example: {codproducto: String}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Petición exitosa
 *      404:
 *        description: Código no encontrado
 */
 server.delete('/producto', validaSesion, validaUsuarioAdmin, (req, res) => {

    const jsonprod = req.body;

    let index = productos.findIndex( (prod) => prod.codproducto === jsonprod.codproducto);
    console.log(index);

    if (index > -1){
        productos.splice(index, 1);

        console.log(`despues: ${JSON.stringify(productos)}`);    
        
        res.send('Petición exitosa');
    }else{
        res.status(404).send('Código no encontrado')
    }
});
/**
 * @swagger
 * /productos:
 *  get:
 *    summary: Lista todos los productos
 *    description: Obtener un listado de todos los productos (sólo usuario Admin puede invocar).
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *
 */
 server.get('/productos', validaSesion, validaUsuarioAdmin, (req, res) => {
    
    res.send(JSON.stringify(productos));
});
/**
 * @swagger
 * /mediopago:
 *  post:
 *    summary: Crear medio de pago
 *    description: Permite crear un medio de pago (sólo usuario Admin).
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un medio de pago.
 *      in: body
 *      required: true
 *      type: string
 *      example: {codmediopago: String, descripcion: String}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Medio de Pago Creado
 *      405:
 *        description: Código existente
 */
 server.post('/mediopago', validaSesion, validaUsuarioAdmin, (req, res) => {

    const jsonmed = req.body;

    if(mediospago.find((med) => med.codmediopago=== jsonmed.codmediopago)){
        res.status(405).send('Código existente');
    }else{
        mediospago.push(jsonmed);
        console.log(`despues: ${JSON.stringify(mediospago)}`);
        
        res.send('Medio de Pago Creado');
    }
});
/**
 * @swagger
 * /mediopago:
 *  put:
 *    summary: Actualiza pedido
 *    description: Permite editar un medio de pago (sólo usuario Admin).
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un medio de pago.
 *      in: body
 *      required: true
 *      type: string
 *      example: {codmediopago: String, descripcion: String}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Petición exitosa
 *      404:
 *        description: Código no encontrado
 */
 server.put('/mediopago', validaSesion, validaUsuarioAdmin, (req, res) => {

    const jsonmed = req.body;

    let index = mediospago.findIndex( (med) => med.codmediopago === jsonmed.codmediopago);

    if (index > -1){
        mediospago[index].descripcion = jsonmed.descripcion;
        console.log(`despues: ${JSON.stringify(mediospago)}`);    
        
        res.send('Petición exitosa');
    }else{
        res.status(404).send('Código no encontrado')
    }
});
/**
 * @swagger
 * /mediopago:
 *  delete:
 *    summary: Elimina medio de pago
 *    description: Permite eliminar un medio de pago (sólo usuario Admin).
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: Cuerpo de un medio de pago.
 *      in: body
 *      required: true
 *      type: string
 *      example: {codmediopago: String}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Petición exitosa
 *      404:
 *        description: Código no encontrado
 */
 server.delete('/mediopago', validaSesion, validaUsuarioAdmin, (req, res) => {

    const jsonmed = req.body;

    let index = mediospago.findIndex( (med) => med.codmediopago === jsonmed.codmediopago);
    console.log(index);

    if (index > -1){
        mediospago.splice(index, 1);

        console.log(`despues: ${JSON.stringify(mediospago)}`);    
        
        res.send('Petición exitosa');
    }else{
        res.status(404).send('Código no encontrado')
    }
});
/**
 * @swagger
 * /mediospago:
 *  get:
 *    summary: Lista medios de pago
 *    description: Obtener un listado con todos los medios de pago (sólo usuario Admin puede invocar).
 *    parameters:
 *    - name: sesionid
 *      description: Id de sesión devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: Peticion exitosa
 *
 */
 server.get('/mediospago', validaSesion, validaUsuarioAdmin, (req, res) => {
    
    res.send(JSON.stringify(mediospago));
});







server.listen(puerto, () => {
    console.log(`Servidor en ejecution en el puerto ${puerto}`);
});