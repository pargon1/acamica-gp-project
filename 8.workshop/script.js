class Usuario {
    constructor(nombre, apellido, mail, pais, contrasena) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.mail = mail;
        this.pais = pais;
        this.contrasena = contrasena; 
    };
};

let usuarios = [];

document.getElementById("botonregistrar").addEventListener("click", function(){

  let inputs = document.getElementsByTagName('input');

  for (let i = 0; i < inputs.length; i++) {
    if (inputs[i].value === '') {
      alert('Debe llenar todos los campos');
      return;
    };
  }
  let pais = document.getElementById("pais").value;

  if (inputs[3].value !== inputs[4].value) {
    alert('Las contraseñas no coinciden');
    return;
  }
  
  let mailnvo = document.getElementById('mail').value;
  
  for (let j = 0; j< usuarios.length; j++){
    if(usuarios[j].mail === mailnvo ){
      alert('Ya existe mail');
      return;
    }
  }

  let nvousuario = new Usuario(inputs[0].value, inputs[1].value, inputs[2].value, pais, inputs[3].value);
  
  console.log(nvousuario);
  console.log(mailnvo);
  
  usuarios.push(nvousuario);

  console.log("termino");
});