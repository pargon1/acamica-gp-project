const ch = require('chalk');

ch.magenta('Hola mundo');

ch.blue('Hola mundo!');

// combinando con estilo y normal
ch.blue('Hola') + 'Mundo' + ch.red('!');

// combinando varios estilos usando la API
ch.blue.bgRed.bold('Hola Mundo!');

// pasando multiples argumentos
ch.blue('Hola', 'Mundo!', 'Foo', 'bar', 'biz', 'baz');

// anidando estilos
ch.red('Hola', ch.underline.bgBlue('mundo') + '!');